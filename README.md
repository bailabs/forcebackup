## Force BackUp

ERPNext/Frappe Force BackUp

#### License

MIT


## Install
```bench get-app forcebackup https://gitlab.com/bailabs/forcebackup.git```

```bench install-app forcebackup```

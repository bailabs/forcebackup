import frappe
import os


def process_task_run():
    path = frappe.get_site_path().replace("./", "")
    if frappe.db.get_value("Force BackUp", None, "run_task") == 'permission':
        frappe.db.set_value("Force BackUp", None, "run_task", "")
        os.system("cd /home/frappe/frappe-bench && /usr/local/bin/bench --site " + path + " reset-perms")

    if frappe.db.get_value("Force BackUp", None, "run_task") == 'backup':
        frappe.db.set_value("Force BackUp", None, "run_task", "")
        os.system("cd /home/frappe/frappe-bench && /usr/local/bin/bench --site " + path + " backup")

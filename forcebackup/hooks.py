# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "forcebackup"
app_title = "Force BackUp"
app_publisher = "Bai Web and Mobile Labs"
app_description = "ERPNext/Frappe Force BackUp"
app_icon = "octicon octicon-database"
app_color = "grey"
app_email = "happy@bai.ph"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/forcebackup/css/forcebackup.css"
# app_include_js = "/assets/forcebackup/js/forcebackup.js"

# include js, css files in header of web template
# web_include_css = "/assets/forcebackup/css/forcebackup.css"
# web_include_js = "/assets/forcebackup/js/forcebackup.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "forcebackup.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "forcebackup.install.before_install"
# after_install = "forcebackup.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "forcebackup.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

scheduler_events = {
    "all": [
        "forcebackup.tasks.process_task_run"
    ],
    # "daily": [
    # 	"forcebackup.tasks.daily"
    # ],
    # "hourly": [
    # 	"forcebackup.tasks.hourly"
    # ],
    # "weekly": [
    # 	"forcebackup.tasks.weekly"
    # ]
    # "monthly": [
    # 	"forcebackup.tasks.monthly"
    # ]
}

# Testing
# -------

# before_tests = "forcebackup.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "forcebackup.event.get_events"
# }

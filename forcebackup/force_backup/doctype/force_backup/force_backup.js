// Copyright (c) 2016, Bai Web and Mobile Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Force BackUp', {
	refresh: function(frm) {

	}
});

frappe.ui.form.on("Force BackUp", "force_backup", function(frm) {
	return  frappe.call({
		method: "forcebackup.events.process_force_backup",
		callback: function(r, rt) {
			if(r.message) {
				frappe.msgprint(r.message['message'], 'Notice')
                        }
			else{
				//console.log('no message')
			}
		}
	});
});

frappe.ui.form.on("Force BackUp", "reset_perm", function(frm) {
	return  frappe.call({
		method: "forcebackup.events.process_reset_permission",
		callback: function(r, rt) {
			if(r.message) {
				frappe.msgprint(r.message['message'], 'Notice')
                        }
			else{
				//console.log('no message')
			}
		}
	});
});

from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        {
            "label": _("Data"),
            "icon": "icon-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Force BackUp",
                    "label": _("Force BackUp"),
                    "description": _("Force BackUp"),
                    "hide_count": True
                }
            ]
        }
    ]
